# Docker + Django + Next.js + Nginx

## docker-compose.yml

```yml
version: "3"

services:
    nginx:
        restart: always
        build:
            dockerfile: Dockerfile
            context: ./nginx
        ports:
            - "5000:80"
    frontend:
        build:
          dockerfile: Dockerfile.dev
          context: ./frontend
        command: npm run dev
        volumes:
            - /app/node_modules # container内のnode_modulesを使用
            - ./frontend:/app
        ports:
            - 3000:3000
        stdin_open: true
```

---

## Note

### relation between nginx and docker-compose

![nginx-default.conf-and-docker-compose-settings](readme/images/nginx-default.conf-and-docker-compose-settings.png)

### react project needs nginx in its direcotry

![need-nginx-in-react-project-directory](readme/images/need-nginx-in-react-project-directory.png)

---
